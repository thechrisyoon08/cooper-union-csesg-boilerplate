import React from 'react';
import { Link } from 'react-router-dom'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import NavButton from "../components/navbutton.js"
class HomePage extends React.Component{
    render(){
	return(
	    <div>
		<h1>
		    Home Page
		</h1>
		<Link to="/login">Login</Link><br />
		<Link to="/signup">Signup</Link>
    
    <ExpansionPanel>
      hello
      <ExpansionPanelSummary>
        detail
      </ExpansionPanelSummary>
    </ExpansionPanel>
	    </div>
	)
    }
};

export default HomePage
